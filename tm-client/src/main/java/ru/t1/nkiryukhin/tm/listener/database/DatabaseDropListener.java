package ru.t1.nkiryukhin.tm.listener.database;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseDropRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

@Component
public final class DatabaseDropListener extends AbstractDatabaseListener {

    public static final String NAME = "database-drop";

    @NotNull
    @Override
    public String getDescription() {
        return "Drop database";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@databaseDropListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATABASE DROP]");
        System.out.println("ENTER PASSPHRASE FOR THIS OPERATION:");
        @NotNull final String passphrase = TerminalUtil.nextLine();
        @NotNull final DatabaseDropRequest request = new DatabaseDropRequest(passphrase);
        adminEndpoint.dropDatabase(request);
        loggerService.info("DATABASE WAS SUCCESSFULLY DROPPED");
    }

}
