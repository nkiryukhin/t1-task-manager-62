package ru.t1.nkiryukhin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;
import ru.t1.nkiryukhin.tm.dto.request.UserUnlockRequest;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

@Component
public final class UserUnlockListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getDescription() {
        return "user unlock";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    @EventListener(condition = "@userUnlockListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();

        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);
        userEndpoint.unlockUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
