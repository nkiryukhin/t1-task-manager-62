package ru.t1.nkiryukhin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface SessionDTORepository extends AbstractDTORepository<SessionDTO> {

    @NotNull
    List<SessionDTO> findByUserId(@NotNull String userId);

}
