package ru.t1.nkiryukhin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectCompleteByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;

    public ProjectCompleteByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
