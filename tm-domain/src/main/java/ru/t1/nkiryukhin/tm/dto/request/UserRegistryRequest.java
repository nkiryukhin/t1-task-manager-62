package ru.t1.nkiryukhin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryRequest extends AbstractUserRequest {

    @NotNull
    private String login;

    @NotNull
    private String password;

    @NotNull
    private String email;

    public UserRegistryRequest(@Nullable final String token) {
        super(token);
    }

}
